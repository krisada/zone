//
//  ZoneTests.swift
//  ZoneTests
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import XCTest
@testable import Zone

class ZoneTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        
        let loginVM = LoginViewModel()
        
//        XCTAssertTrue(loginVM.login())
        XCTAssertFalse(loginVM.enableLogin.value)
        
        loginVM.username.value = "0910002109"
        
        XCTAssertFalse(loginVM.enableLogin.value)
        XCTAssertTrue(loginVM.usernameValidated.value)
        
        loginVM.password.value = "09100"
        
        XCTAssertFalse(loginVM.enableLogin.value)
        XCTAssertFalse(loginVM.passwordValidated.value)
        
        loginVM.password.value = "09100ssdsdddsd"
        
        XCTAssertTrue(loginVM.usernameValidated.value)
        XCTAssertTrue(loginVM.enableLogin.value)
        
        XCTAssertTrue(loginVM.username.value == "0910002109")
        XCTAssertTrue(loginVM.password.value == "09100ssdsdddsd")
        
    }
    
    func testRegister() {
        
        let registerVM = RegisterViewModel()
        
        XCTAssertFalse(registerVM.firstNameValidated.value)
        registerVM.firstName.value = "df"
        XCTAssertTrue(registerVM.firstNameValidated.value)
        
        
        // Last name //
        
        XCTAssertFalse(registerVM.lastNameValidated.value)
        registerVM.lastName.value = " kris ada"
        XCTAssertFalse(registerVM.lastNameValidated.value, "Not allow space on the first position")
        registerVM.lastName.value = "5kris ada"
        XCTAssertFalse(registerVM.lastNameValidated.value, "Not allow digit on the first position")
        registerVM.lastName.value = "กฤษดา แก้วจันทร์ฉาย"
        XCTAssertTrue(registerVM.lastNameValidated.value)
        
        
        // Mobile Numebr //

        XCTAssertFalse(registerVM.mobileNumberValidated.value, "")
        registerVM.mobileNumber.value = "0910032109"
        XCTAssertTrue(registerVM.mobileNumberValidated.value)
        
        
        // Password //
        
        XCTAssertFalse(registerVM.passwordValidated.value, "")
        registerVM.password.value = "0123234"
        XCTAssertTrue(registerVM.passwordValidated.value)
        
        
        // Register Enable //
        XCTAssertTrue(registerVM.enableRegister.value)
        
    }
    
    func testFeedView() {
        
        // Setup 'Zone' for test //
        let zone = Zone.CurrentLocation(100,70)
        
        let feedVM = FeedViewModel(zone: zone)
        XCTAssertTrue(feedVM.sections.count == 5)
        
        let board = feedVM.sections[0]
        XCTAssertTrue(board.title == "BOARD")
        XCTAssertTrue(board.canPost)
        board.canPost = false
        
        XCTAssertTrue(feedVM.sections[1].title == "NEWS")
        XCTAssertFalse(feedVM.sections[1].canPost)
        XCTAssertTrue(board.contents.count == 8)
        board.contents = []
        XCTAssertEqual(0, board.contents.count)
        
        let newZone = Zone.Zone("test")
        feedVM.zone = newZone
        XCTAssertEqual(false, board.canPost)
        XCTAssertEqual(8, board.contents.count)
        
    }
    
    func testFeedModel() {
        
        // XCode Bug
        // Unrelated XCUIElement warning and cast from AnyObject? to "any other type" that fails only in test target
        // http://www.openradar.me/22836823
        
        var feeds: [FeedContent] = []
        feeds.append(PostContent(id: "00", username: "Krisada Keawjunchai", type: "Lifestyle", time: "2 hours ago", message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...", likes: 0, comments: 231, userImageURL: "1", imageURL: "2"))
        guard let feed = feeds.first, let postContent = feed as? PostContent else {
            fatalError("Can't cast feed to PostContent")
        }
        XCTAssertTrue(postContent.feedID == "00")
        XCTAssertTrue(postContent.username == "Krisada Keawjunchai")
        XCTAssertTrue(postContent.contentType == "Lifestyle")
        XCTAssertTrue(postContent.time == "2 hours ago")
        XCTAssertTrue(postContent.message == "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...")
        XCTAssertTrue(postContent.likes == 0)
        XCTAssertTrue(postContent.comments == 231)
        XCTAssertTrue(postContent.userImageURL == "1")
        XCTAssertTrue(postContent.imageURL == "2")
        
        XCTAssertEqual(postContent.isLiked, false)
        postContent.like()
        XCTAssertEqual(postContent.likes, 1)
        XCTAssertEqual(postContent.isLiked, true)
        postContent.like()
        XCTAssertEqual(postContent.likes, 0)
        XCTAssertEqual(postContent.isLiked, false)
        
        // Exteam case for like function //
        postContent.isLiked = true
        XCTAssertEqual(postContent.isLiked, true)
        postContent.like()
        XCTAssertEqual(postContent.likes, 0)
        XCTAssertEqual(postContent.isLiked, false)
        
    }
    
    
}
