//
//  LoginTests.swift
//  Zone
//
//  Created by Developer on 10/19/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import XCTest
@testable import Zone

class LoginTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidateUsername() {
        let registerVM = LoginViewModel()
        
        XCTAssertTrue(registerVM.validate(username: "0910032109"), "")
        XCTAssertFalse(registerVM.validate(username: "091003210"), "Phone number much equal 10 chars")
        XCTAssertFalse(registerVM.validate(username: "091003210d9"), "Phone number should not have latter")
        XCTAssertFalse(registerVM.validate(username: "r910032109D"), "Phone number should not have latter")
        XCTAssertFalse(registerVM.validate(username: ""), "Phone number should not have latter")
    }
    
}
