//
//  FeedSectionTableViewController.swift
//  Zone
//
//  Created by Developer on 11/5/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


enum FeedSection: Int {
    
    case Filter
 
    case Content
    
}



class FeedSectionTableViewController: UITableViewController {
    
    var viewModel: FeedSectionViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        
        viewModel?.completeLoaded = {
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch FeedSection(rawValue: section)! {
        case .Filter:
            return 1
        case .Content:
            return viewModel?.contents.count ?? 0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch FeedSection(rawValue: indexPath.section)! {
            
        case .Filter:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Post", for: indexPath) as! PostFilterTableViewCell
            cell.postView.isHidden = !viewModel.canPost
            return cell
            
        case .Content:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailWithImage", for: indexPath)
            
            switch cell {
            case let postCell as PostTableViewCell:
                if let post = viewModel.contents[indexPath.row] as? PostContent {
                    postCell.render(post: post)
                    postCell.delegate = self
                }
                return postCell
            default:
                return cell
            }
        }
    }
}


