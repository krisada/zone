//
//  PostTableViewCell.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/8/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import SDWebImage


protocol PostCellDelegate {
    
    func postCellDidShare(items: [AnyObject])
    func postCellDidLike()
    func postCellDidComment()
}


class PostTableViewCell: UITableViewCell {
    
    static let kImageHeight: CGFloat = 250
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var contentImageView: UIImageView!
    
    @IBOutlet weak var contentImageViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var likesLabel: UILabel!
    
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    
    var content: PostContent!
    
    var delegate: PostCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func render(post: PostContent) {
        self.content = post
        usernameLabel.text = content.username
        messageLabel.text = content.message
        timeLabel.text = content.time
        likesLabel.text = "\(content.likes)"
        commentsLabel.text = "\(content.comments)"
        typeLabel.text = content.contentType
        
        if let imageString = content.imageURL, let url = URL(string: imageString) {
            contentImageView.sd_setImage(with: url)
            contentImageViewHeight.constant = PostTableViewCell.kImageHeight;
        } else {
            contentImageViewHeight.constant = 0;
        }
        
        if let imageString = content.userImageURL, let url = URL(string: imageString) {
            userImageView.sd_setImage(with: url)
        }
    }
    
    @IBAction func share(_ sender: Any) {
        let items: [Any] = [
            content.message,
            contentImageView.image ?? ""
        ]
        delegate?.postCellDidShare(items: items as [AnyObject])
    }
    
    @IBAction func like(_ sender: Any) {
        delegate?.postCellDidLike()
    }

    @IBAction func comment(_ sender: Any) {
        delegate?.postCellDidComment()
    }

}
