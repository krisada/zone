//
//  SettingMenuTableViewCell.swift
//  Zone
//
//  Created by Developer on 11/13/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit

class SettingMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    static let CellID = "SettingMenuCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
