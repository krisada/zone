//
//  PostContent.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation


class PostContent: FeedContent {
    
    var username = ""
    
    var userImageURL: String?
    
    var imageURL: String?
    
    var message = ""
    
    var contentType = ""
    
    var time = ""
    
    var likes = 0
    
    var comments = 0
    
    var isLiked = false
    
    
    init(id: String, username: String, type: String, time: String, message: String, likes: Int, comments: Int, userImageURL: String , imageURL: String) {
        
        self.username = username
        self.contentType = type
        self.time = time
        self.message = message
        self.likes = likes
        self.comments = comments
        self.imageURL = imageURL
        self.userImageURL = userImageURL
        
        super.init(id: id)
        
    }
    
    init(card: Card) {
        
        self.username = card.username ?? ""
        self.contentType = card.category ?? ""
        self.time = ""
        self.message = card.content ?? ""
        self.likes = card.likes ?? 0
        self.comments = card.comments ?? 0
        self.imageURL = card.image ?? ""
        self.userImageURL = card.userImage ?? ""
        super.init(id: "\(card.id)")
    }
    
    
    func like() {
        
        if !isLiked {
            self.likes += 1
        } else {
            self.likes -= self.likes == 0 ? 0 : 1
        }
        isLiked = !isLiked
        
    }
    
}
