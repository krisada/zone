//
//  User.swift
//  Zone
//
//  Created by Apple on 1/12/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ReactiveKit


class User: NSObject {
    
    var complete: ((Void) -> Void)?
    
    static var shareInstance = User()
    
    var userMe: UserMe?
    
}
