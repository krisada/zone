//
//  FeedViewModel.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation
import UIKit


enum Zone {
    
    case CurrentLocation(String ,Double, Double)
    
    case Country(Float, Float)

    case Zone(String)
    
    case Test()
}


class FeedViewModel {
    
    var sections: [FeedSectionViewModel] = []  // List of FeedSectionTableViewController
    
    var zone: Zone?
    
    var zoneList: [Zone] = []
    
    init() {
        
        /* Setup Each Section */
        let board = FeedSectionViewModel(section: .Board, postable: true)
        let news = FeedSectionViewModel(section: .News, postable: false)
        let food = FeedSectionViewModel(section: .Food, postable: false)
        let biz = FeedSectionViewModel(section: .Biz, postable: false)
        let give = FeedSectionViewModel(section: .Give, postable: false)
        
        self.sections = [board, news, food, biz, give]
    }
    
    func loadData(zone: Zone) {
        switch zone {
        case .CurrentLocation(let id,let lat, let lon):
            _ = self.sections.map { $0.loadContent(id: id, latitude: lat, longitude: lon) }
            break
        case .Test():
            _ = self.sections.map { $0.loadContent(test: true) }
            break
        default:
            _ = self.sections.map { $0.loadContent(test: false) }
            break
        }
        
    }
    
    convenience init(zone: Zone) {
        
        self.init()
        self.zone = zone
    }
    
}
