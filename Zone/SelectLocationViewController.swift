//
//  SelectLocationViewController.swift
//  Zone
//
//  Created by Developer on 11/6/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


enum SelectLocationSection: Int {
    
    case header
    
    case first
    
    case content
    
    case last
    
    // Factory //
    func getCell(from table: UITableView) -> UITableViewCell? {
        
        switch self {
            
        case .header:
            guard let cell = table.dequeueReusableCell(withIdentifier: "firstMenuCellIdentifier") else {
                return nil
            }
            return cell
            
        case .first:
            guard let cell = table.dequeueReusableCell(withIdentifier: "secondMenuCellIdentifier") else {
                return nil
            }
            return cell
        
        case .content:
            guard let cell = table.dequeueReusableCell(withIdentifier: "regularMenuCellIdentifier")  else {
                return nil
            }
            return cell
            
        case .last:
            guard let cell = table.dequeueReusableCell(withIdentifier: "lastMenuCellIdentifier")  else {
                return nil
            }
            return cell
        }
    }
    
}


class SelectLocationViewController: UIViewController {
    
    var contextMenuTableView: YALContextMenuTableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTable()
        perform(#selector(SelectLocationViewController.setupYALContextMenu), with: self, afterDelay: 3)
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - UITableViewDataSource & UITableViewDelegate
//extension SelectLocationViewController: UITableViewDataSource, UITableViewDelegate {
//    
//    func setupTable() {
//        
//        contextMenuTableView.estimatedRowHeight = 44
//        contextMenuTableView.rowHeight = UITableViewAutomaticDimension
//        
//        contextMenuTableView.delegate = self
//        contextMenuTableView.dataSource = self
//        
//    }
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 4
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch SelectLocationSection(rawValue: section)! {
//        case .content:
//            return 5
//        default:
//            return 1
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let section = SelectLocationSection(rawValue: indexPath.section), let cell = section.getCell(from: tableView) else {
//            return UITableViewCell()
//        }
//        return cell
//    }
//    
//}


// MARK: - YALContextMenu
extension SelectLocationViewController: YALContextMenuTableViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    func setupTable() {

        contextMenuTableView.estimatedRowHeight = 44
        contextMenuTableView.rowHeight = UITableViewAutomaticDimension

        contextMenuTableView.delegate = self
        contextMenuTableView.dataSource = self

    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch SelectLocationSection(rawValue: section)! {
        case .content:
            return 5
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = SelectLocationSection(rawValue: indexPath.section), let cell = section.getCell(from: tableView) else {
            return UITableViewCell()
        }
        return cell
    }
    
    func setupYALContextMenu() {
        contextMenuTableView = YALContextMenuTableView(tableViewDelegateDataSource: self)
        contextMenuTableView.animationDuration = 0.15
        contextMenuTableView.yalDelegate = self
        contextMenuTableView.isScrollEnabled = false
        contextMenuTableView.show(in: self.navigationController?.view, with: .zero, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let t = tableView as! YALContextMenuTableView
        t.dismis(with: indexPath)
        
    }
    
//    func tableView(tableView: YALContextMenuTableView, numberOfRowsInSection section: Int) -> Int {
//        switch SelectLocationSection(rawValue: section)! {
//        case .content:
//            return 5
//        default:
//            return 1
//        }
//    }
//    
//    func tableView(tableView: YALContextMenuTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let section = SelectLocationSection(rawValue: indexPath.section), let cell = section.getCell(from: tableView) else {
//            return UITableViewCell()
//        }
//        return cell
//    }
    
    func contextMenuTableView(_ contextMenuTableView: YALContextMenuTableView!, didDismissWith indexPath: IndexPath!) {
        
    }
    
}
