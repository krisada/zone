//
//  UserMe.swift
//  Zone
//
//  Created by Apple on 1/12/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveKit


class UserMe: Mappable {
    
    var id: Int?
    var email: String?
    var telephone: String?
    var first = Property("")
    var last = Property("")
    var imageProfile = Property("")
    var imageCover: String?
    var gender: String?
    var birthday: String?
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        id <- map["data.id"]
        email <- map["data.email"]
        telephone <- map["data.telephone"]
        gender <- map["data.profile.gender"]
        first.value <- map["data.profile.first_name"]
        last.value <- map["data.profile.last_name"]
        imageProfile.value <- map["data.profile.picture.src"]
        imageCover <- map["data.cover.src"]
    }
}
