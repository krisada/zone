//
//  AppDelegate.swift
//  Zone
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


var locationQueue = OperationQueue()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        
        /* Start location manager */
        locationQueue.maxConcurrentOperationCount = 1
        
        // Load User Profile //
        if (UserDefaults.getAccessToken() != nil) {
            let response = ZoneAPI.requestSync(ZoneURL.userProfile, method: .get, mappableType: UserMe.self)
            if let userMe = response as? UserMe, let _ = userMe.id {
                User.shareInstance.userMe = userMe
                User.shareInstance.complete?()
                
                /* Go to Main App */
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateInitialViewController()
                goto(vc!)
            } else {
                
                /* Go to Login Page */
                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let vc = storyboard.instantiateInitialViewController()
                goto(vc!)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


extension AppDelegate {
    
    func goto(_ vc: UIViewController) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
}

