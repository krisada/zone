//
//  Location.swift
//  Zone
//
//  Created by Apple on 1/24/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveKit

class Stack: Mappable {
    
    class Location: Mappable {
        
        var id: String?
        var name: String?
        var type: String?
        var views: Int?
        var members: Int?
        var image: String?
        var stack: Stack?
        
        
        required init?(map: Map) {
            mapping(map:map)
        }
        
        func mapping(map: Map) {
            id <- map["id"]
            name <- map["name"]
            type <- map["type"]
        }
    }
    
    var country: Location?
    var province: Location?
    var district: Location?
    var subdistrict: Location?
    var zone: Location?
    var area: Location?
    var place: Location?
    
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        country <- map["country"]
        province <- map["province"]
        district <- map["district"]
        subdistrict <- map["subdistrict"]
        zone <- map["zone"]
        area <- map["area"]
        place <- map["place"]
    }
}

class Location: Mappable {

    var id: String?
    var name = Property("")
    var type: String?
    var views: Int?
    var members = Property(0)
    var image = Property("")
    var stack: Stack?
    
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        id <- map["data.id"]
        name.value <- map["data.name"]
        type <- map["data.type"]
        views <- map["data.view_count"]
        members.value <- map["data.member_count"]
        image.value <- map["data.image.src"]
        stack <- map["data.stack"]
    }
}
