//
//  RegisterViewModel.swift
//  Zone
//
//  Created by Developer on 10/21/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

class RegisterViewModel {
    
    var firstName = Observable<String?>("")
    var lastName = Observable<String?>("")
    var mobileNumber = Observable<String?>("")
    var password = Observable<String?>("")
    
    var firstNameValidated = Observable<Bool>(false)
    var lastNameValidated = Observable<Bool>(false)
    var mobileNumberValidated = Observable<Bool>(false)
    var passwordValidated = Observable<Bool>(false)
    
    var enableRegister = Observable<Bool>(false)
    
    
    init() {
        
        firstName
            .map { ($0?.isName())! }
            .bind(to: firstNameValidated)
        
        lastName
            .map { ($0?.isName())! }
            .bind(to: lastNameValidated)
        
        mobileNumber
            .map { ($0?.isPhone())! }
            .bind(to: mobileNumberValidated)
        
        password
            .map { ($0?.isPassword())! }
            .bind(to: passwordValidated)
        
        combineLatest(firstNameValidated, lastNameValidated, mobileNumberValidated, passwordValidated) { $0 && $1 && $2 && $3 }
            .bind(to: enableRegister)
        
    }
    
}


