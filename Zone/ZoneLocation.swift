//
//  ExploreLocation.swift
//  Zone
//
//  Created by Developer on 1/24/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire


/*  */
class ZoneLocation: AsyncOperation {
    
    var latitude: Double?
    var longitude: Double?
    
    var location: Location?
    
    override init() {
        super.init()
    }
    
    override func main() {
        if let lat = CurrentLocationManager.currentLocation?.coordinate.latitude, let lon = CurrentLocationManager.currentLocation?.coordinate.longitude {
            let parameters: Parameters = [
                "latitude" : lat,
                "longitude" : lon,
                "type" : "area"
            ]
            ZoneAPI.request(ZoneURL.location, mappableType: Location.self, parameters: parameters) { (obj, error) in
                if let location = obj as? Location {
                    self.location = location
                    self.state = .Finished
                }
            }
        }
        
    }
}
