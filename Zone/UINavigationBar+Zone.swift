//
//  File.swift
//  Zone
//
//  Created by Developer on 10/21/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
extension UINavigationBar {
    
    @IBInspectable
    public var hideBG: Bool {
        set {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
            self.isTranslucent = true
        }
        
        get {
            return self.isTranslucent
        }
    }
}
