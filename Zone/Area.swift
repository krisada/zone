//
//  Area.swift
//  Zone
//
//  Created by Developer on 1/10/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper


class Area: Mappable {
    
    var id: String?
    var type: String?
    var name: String?
    var nameTH: String?
    var nameEN: String?
    var image: String?
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        id <- map["data.id"]
        type <- map["data.type"]
        name <- map["data.name"]
        nameEN <- map["data.display_name.en"]
        nameTH <- map["data.display_name.th"]
        image <- map["data.image.src"]
    }
}
