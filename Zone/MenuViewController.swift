//
//  MenuViewController.swift
//  Zone
//
//  Created by Developer on 11/13/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond


enum MenuSection: Int {
    
    case PrivateGroup
    
    
    case Setting
    
    
    static var count: Int {
        
        return MenuSection.Setting.hashValue + 1
    }
    
    var numberOfCell: Int {
        
        switch self {
        case .PrivateGroup:
            return 0
        case .Setting:
            return 4
        }
    }
    
    var title: String {
        
        switch self {
        case .PrivateGroup:
            return "Private Group"
        case .Setting:
            return "Settings"
        }
    }
    
}

enum MenuSetting: Int {
    
    case Profile
    
    case Application
    
    case HelpSupport
    
    case About
    
    
    var name: String {
        
        switch self {
        case .Profile:
            return "Profile Settings"
        case .Application:
            return "Application Settings"
        case .HelpSupport :
            return "Help and Support"
        case .About:
            return "About ZONE"
        }
    }
    
    var image: UIImage {
        
        switch self {
        case .Profile:
            return #imageLiteral(resourceName: "user_setting")
        case .About:
            return #imageLiteral(resourceName: "about")
        case .Application:
            return #imageLiteral(resourceName: "application_setting")
        case .HelpSupport :
            return #imageLiteral(resourceName: "help")
        }
    }
    
    var viewController: UIViewController {
        
        switch self {
        case .Profile:
            return UIViewController()
        case .About:
            let aboutStoryboard = UIStoryboard(name: "About", bundle: nil)
            guard let vc = aboutStoryboard.instantiateInitialViewController() else {
                return UIViewController()
            }
            return vc
        case .Application:
            let appSettingStoryboard = UIStoryboard(name: "ApplicationSetting", bundle: nil)
            guard let vc = appSettingStoryboard.instantiateInitialViewController() else {
                return UIViewController()
            }
            return vc
        case .HelpSupport :
            let helptStoryboard = UIStoryboard(name: "Help", bundle: nil)
            guard let vc = helptStoryboard.instantiateInitialViewController() else {
                return UIViewController()
            }
            return vc
        }
    }
    
}

protocol MenuViewControllerDelegate {
    
    func menuDidSelect(menu: MenuSetting)
}


class MenuViewController: UIViewController {
    
    @IBOutlet weak var imageProfileView: UIImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: MenuViewControllerDelegate?
    
    
    @IBAction func logout(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        binding()

        // Do any additional setup after loading the view.
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MenuViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return MenuSection.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return MenuSection(rawValue: section)!.title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuSection(rawValue: section)!.numberOfCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingMenuTableViewCell.CellID, for: indexPath)
        
        switch cell {
        case let settingCell as SettingMenuTableViewCell:
            let menu = MenuSetting(rawValue: indexPath.row)!
            settingCell.titleLable.text = menu.name
            settingCell.iconImageView.image = menu.image
            return settingCell
        default:
            return cell
        }
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        guard let header = view as? UITableViewHeaderFooterView else {
            return
        }
        header.textLabel?.tintColor = UIColor.zoneWarmGrey
        header.textLabel?.font = UIFont.zoneBodyBoldThaiFont()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch MenuSection(rawValue: indexPath.section)! {
        case .PrivateGroup:
            break
        case .Setting:
            dismiss(animated: true, completion: { [unowned self] in
                self.delegate?.menuDidSelect(menu: MenuSetting(rawValue: indexPath.row)!)
            })
            
            break
        }
        
    }
}

extension MenuViewController {
    
    func binding() {
        bnd_bag.add(disposable:(User.shareInstance.userMe?.imageProfile.observeNext { [unowned self] value in
            if let url = URL(string: value) {
                DispatchQueue.main.async {
                    self.imageProfileView.sd_setImage(with: url)
                }
            }
        })!)
//        combineLatest(with:(User.shareInstance.userMe?.first, User.shareInstance.userMe?.last))
    }
}
