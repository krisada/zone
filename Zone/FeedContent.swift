//
//  FeedContent.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation

enum FeedContentType: Int {
    
    case Post
    
    case News
    
    case Unknown
    
}

class FeedContent {
    
    var feedID: String = ""
    
    init(id: String) {
        self.feedID = id
    }
        
}
