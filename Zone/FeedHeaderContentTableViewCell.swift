//
//  FeedHeaderContentTableViewCell.swift
//  Zone
//
//  Created by Developer on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit

class FeedHeaderContentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension FeedHeaderContentTableViewCell: YALContextMenuCell {
    
    func animatedIcon() -> UIView {
        return moreButton
    }
    
    func animatedContent() -> UIView {
        return nameLabel
    }
}
