//
//  ApplicationSettingTableViewController.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/15/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit

class ApplicationSettingTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hideBG = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
