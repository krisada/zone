//
//  RegisterViewModel.swift
//  Zone
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import Bond
import ReactiveKit


class LoginViewModel: NSObject {
    
    var username = Observable<String?>("")
    var password = Observable<String?>("")
    
    var queue = OperationQueue()
    var user: Authenticate?
    
    var usernameValidated = Observable<Bool>(false)
    var passwordValidated = Observable<Bool>(false)
    var enableLogin = Observable<Bool>(false)
    
    override init() {
        super.init()
        
        username
            .map { ($0?.isPhone())! }
            .bind(to: usernameValidated)
        
        password
            .map { ($0?.isPassword())! }
            .bind(to: passwordValidated)
        
        combineLatest(usernameValidated, passwordValidated, combine: { $0 && $1 } )
            .bind(to: enableLogin)
        
    }
    
    func login() {
        self.user = Authenticate(user: username.value!, pass: password.value!)
        queue.addOperation(self.user!)
    }
    
}

