//
//  RegisterViewController.swift
//  Zone
//
//  Created by Developer on 10/23/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import Bond
import ReactiveKit


class RegisterViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var passwordInfoButton: UIButton!
    @IBOutlet weak var passwordShowButton: UIButton!
    @IBOutlet weak var phoneLoading: UIActivityIndicatorView!
    @IBOutlet weak var phoneCheckImage: UIImageView!
    @IBOutlet var passwordTextField: UITextField!
    
    var registerViewModel = RegisterViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        binding()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension RegisterViewController {
    
    func binding() {
        
        // Binding
        registerViewModel.firstName.bidirectionalBind(to: firstNameTextField.bnd_text)
        registerViewModel.lastName.bidirectionalBind(to: lastNameTextField.bnd_text)
        registerViewModel.mobileNumber.bidirectionalBind(to: phoneTextField.bnd_text)
        registerViewModel.password.bidirectionalBind(to: passwordTextField.bnd_text)
        
        // Update UI //
        registerViewModel.firstNameValidated
            .map{ $0 ? UIColor.white : UIColor.gray }
            .bind(to: firstNameTextField.bnd_textColor)
        
        registerViewModel.lastNameValidated
            .map{ $0 ? UIColor.white : UIColor.gray }
            .bind(to: lastNameTextField.bnd_textColor)
        
        registerViewModel.mobileNumberValidated
            .map{ $0 ? UIColor.white : UIColor.gray }
            .bind(to: phoneTextField.bnd_textColor)
        
        registerViewModel.passwordValidated
            .map{ $0 ? UIColor.white : UIColor.gray }
            .bind(to: passwordTextField.bnd_textColor)
        
        bnd_bag.add(disposable:registerViewModel.enableRegister
            .observeNext { [unowned self] ( result ) in
                let color = result ? UIColor.zoneSquash : UIColor.zoneSilver
                let textColor = result ? UIColor.zoneBlack : UIColor.zoneBlack
                self.signupButton.borderColor = result ? UIColor.zoneSquash : UIColor.zoneSilver
                self.signupButton.backgroundColor = color
                self.signupButton.setTitleColor(textColor, for: .normal)
        })
        
    }
}
