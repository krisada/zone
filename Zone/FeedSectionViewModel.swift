//
//  FeedSectionViewModel.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


typealias FeedResult = () -> [FeedContent]


enum FeedContentSection {
    
    case Board
    
    case News
    
    case Food
    
    case Biz
    
    case Give
    
    
    var title: String {
        
        switch self {
            
        case .Board:
            return "BOARD"
           
        case .News:
            return "NEWS"
            
        case .Food:
            return "FOOD"
            
        case .Biz:
            return "BIZ"
            
        case .Give:
            return "GIVE"
        }
    }
    
    var apiSuffix: String {
            
        
        switch self {
    
        case .Board:
            return "general"
            
        case .News:
            return "news"
            
        case .Food:
            return "food"
            
        case .Biz:
            return "shop"
            
        case .Give:
            return "give"
        
        }
    }
}


class FeedSectionViewModel {
    
    var title: String {
        return section.title
    }
    
    var apiSuffix: String {
        return section.apiSuffix
    }
    
    var completeLoaded: ((Void) -> Void)?
    
    private var section: FeedContentSection!
    
    var canPost = false
    
    var contents:[FeedContent] = []
    
    init(section: FeedContentSection, postable: Bool) {
        
        self.section = section
        self.canPost = postable
    }
}


extension FeedSectionViewModel {
    
    func loadContent(result: FeedResult = { [] }) {
        contents = result()
    }
    
    func loadContent(id: String, latitude: Double, longitude: Double) {
        let parameters: Parameters = ["level_type" : "area",
                                      "level_id" : id,
                                      "latitude" : latitude,
                                      "longitude" : longitude,
                                      "type" : apiSuffix
        ]
        ZoneAPI.request(ZoneURL.feed, mappableType: Feed.self, parameters: parameters) { (obj: Mappable?, error: NSError?) in
            
            if let feed = obj as? Feed, let cards = feed.cards {
                for card in cards {
                    self.contents.append(PostContent(card: card))
                }
            }
            self.completeLoaded?()
        }
    }
    
    func loadContent(test: Bool = false) {
        
        if !test {
            contents = []
        } else {
            contents = [
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหา",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "",
                            imageURL: "https://static.pexels.com/photos/225232/pexels-photo-225232-large.jpeg"),
                PostContent(id: "00",
                            username: "Dominic Cooper",
                            type: "News",
                            time: "3 hours ago",
                            message: "NASA Mars Orbiter Preparing for Mars Lander's 2016 Arrivalpor ...",
                            likes: 32,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/27411/pexels-photo-27411-medium.jpg",
                            imageURL: "https://static.pexels.com/photos/224198/pexels-photo-224198-medium.jpeg"),
                PostContent(id: "00",
                            username: "Dominic Cooper",
                            type: "News",
                            time: "5 hours ago",
                            message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu ...",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/51969/model-female-girl-beautiful-51969-medium.jpeg",
                            imageURL: "https://static.pexels.com/photos/196037/pexels-photo-196037-large.jpeg"),
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...",
                            likes: 12,
                            comments: 3,
                            userImageURL: "https://static.pexels.com/photos/7529/pexels-photo-medium.jpeg",
                            imageURL: "https://static.pexels.com/photos/215482/pexels-photo-215482-medium.jpeg"),
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/61100/pexels-photo-61100-medium.jpeg",
                            imageURL: "https://static.pexels.com/photos/9655/pexels-photo-9655-medium.jpeg"),
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/173295/pexels-photo-173295-medium.jpeg",
                            imageURL: "https://static.pexels.com/photos/167886/pexels-photo-167886-medium.jpeg"),
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/211050/pexels-photo-211050-medium.jpeg",
                            imageURL: "https://static.pexels.com/photos/146737/pexels-photo-146737-medium.jpeg"),
                PostContent(id: "00",
                            username: "Krisada Keawjunchai",
                            type: "Lifestyle",
                            time: "2 hours ago",
                            message: "เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อห...",
                            likes: 1099,
                            comments: 231,
                            userImageURL: "https://static.pexels.com/photos/25845/pexels-photo-25845-medium.jpg",
                            imageURL: "https://static.pexels.com/photos/5882/vintage-brown-bike-bicycle-5882-medium.jpg")
            ]
        }
    
    }
    
}
