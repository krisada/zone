//
//  Result.swift
//  Zone
//
//  Created by Developer on 10/20/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation

enum Result<T> {
    case Success(T)
    case Error(ErrorType)
}
