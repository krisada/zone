//
//  UIButton + Zone.swift
//  Zone
//
//  Created by Developer on 11/6/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    
    @IBInspectable var placehoderColor: UIColor {
        get {
            return self.placehoderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue])
        }
    }
    
}
