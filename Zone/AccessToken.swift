//
//  AccessToken.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 1/9/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper


class AccessToken: Mappable {
    
    var accessToken: String?
    var refreshToken: String?
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        accessToken <- map["data.access_token"]
        refreshToken <- map["data.refresh_token"]
    }
}
