//
//  ZoneAPI.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 1/9/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_Synchronous
import ObjectMapper


public typealias ApiCompletionBlock = (_ result: Mappable?, _ error: NSError?) -> Void


struct ZoneAPI {
    
    static func request(_ url: URLConvertible,
                        method: HTTPMethod = .get,
                        mappableType: Mappable.Type,
                        parameters: Parameters? = nil,
                        completeBlock: @escaping ApiCompletionBlock
                        )
    {
        var header: [String : String] = Dictionary()
        if let token = UserDefaults.getAccessToken() {
            header["Authorization"] = "Bearer \(token)"
        }
        
        Alamofire
            .request(url, method: method, parameters: parameters, headers: header)
            .responseJSON { (response) in
            switch response.result {
            case .success(let responseJSON):
                if let obj = mappableType.init(map: Map(mappingType: .fromJSON, JSON: responseJSON as! [String : Any])) {
                    completeBlock(obj, nil)
                } else {
                    completeBlock(nil, nil)
                }
            case .failure(let err):
                print(err)
                completeBlock(nil, ZoneErrorType.Error(err as! String).error)
            }
            
        }
    }
    
    static func requestSync(_ url: URLConvertible,
                        method: HTTPMethod = .get,
                        mappableType: Mappable.Type,
                        parameters: Parameters? = nil
        ) -> Mappable?
    {
        var header: [String : String] = Dictionary()
        if let token = UserDefaults.getAccessToken() {
            header["Authorization"] = "Bearer \(token)"
        }
        
        let response = Alamofire
            .request(url, method: method, parameters: parameters, headers: header)
            .responseJSON()
        
        switch response.result {
        case .success(let responseJSON):
            if let obj = mappableType.init(map: Map(mappingType: .fromJSON, JSON: responseJSON as! [String : Any])) {
                return obj
            } else {
                return nil
            }
        case .failure(let err):
            print(err)
            return nil
//            return ZoneErrorType.Error(err as! String).error
        }
    }
}
