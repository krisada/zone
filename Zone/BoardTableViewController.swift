//
//  BoardTableViewController.swift
//  Zone
//
//  Created by Developer on 11/5/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit



class BoardTableViewController: FeedSectionTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

extension FeedSectionTableViewController: PostCellDelegate {
    
    func postCellDidLike() {
        
    }
    
    func postCellDidComment() {
        
    }
    
    func postCellDidShare(items: [AnyObject]) {
        // exclude some activity types from the list (optional)
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
