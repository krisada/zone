//
//  Feed.swift
//  Zone
//
//  Created by Developer on 1/10/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper


class Feed: Mappable {
    
    var cards: [Card]?
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        cards <- map["data"]
    }
}
