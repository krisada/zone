//
//  FeedHeaderSecondTableViewCell.swift
//  Zone
//
//  Created by Developer on 11/7/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


class FeedHeaderSecondTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension FeedHeaderSecondTableViewCell: YALContextMenuCell {
    
    func animatedIcon() -> UIView {
        return moreButton
    }
    
    func animatedContent() -> UIView {
        return labelName
    }
}
