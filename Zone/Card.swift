//
//  Feed.swift
//  Zone
//
//  Created by Developer on 1/10/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import ObjectMapper


class Card: Mappable {
    
    var id: Int?
    var likes: Int?
    var comments: Int?
    var createDate: Date?
    var shareCount: Int?
    var image: String?
    var views: Int?
    var liked: Bool?
    var pined: Bool?
    var owner: Bool?
    var content: String?
    var topic: String?
    var username: String?
    var userImage: String?
    var category: String?
    
    required init?(map: Map) {
        mapping(map:map)
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        comments <- map["comment_count"]
        createDate <- (map["datetime_create"] ,DateTransform())
        shareCount <- map["share_count"]
        views <- map["view_count"]
        image <- map["media.0.src"]
        liked <- map["is_liked"]
        pined <- map["is_pinned"]
        owner <- map["is_owner"]
        content <- map["content"]
        topic <- map["topic"]
        likes <- map["like_count"]
        username <- map["owner.name"]
        userImage <- map["owner.picture.src"]
        category <- map["category_child.name"]
    }
}
