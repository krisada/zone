//
//  LoginViewController.swift
//  Zone
//
//  Created by Developer on 10/20/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import Bond
import ReactiveKit
import Alamofire
import ObjectMapper


class LoginViewController: UIViewController {
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var keyboardConstraint: NSLayoutConstraint!
    
    
    var keyboardRect = CGRect() {
        
        willSet {
            keyboardConstraint.constant = newValue.height + 95.0
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) { [unowned self] in
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    
    let viewModel = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        binding()
        
        self.viewModel.username.value = "0910032109"
        self.viewModel.password.value = "ball1234"
        
        bnd_bag.add(disposable: NotificationCenter.default.bnd_notification(name: Notification.Name.UIKeyboardDidShow).observeNext { [unowned self] (noti) in
            guard let value = noti.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue else {
                return
            }
            self.keyboardRect = value.cgRectValue
        })
        
        bnd_bag.add(disposable: NotificationCenter.default.bnd_notification(name: Notification.Name.UIKeyboardDidHide).observeNext { [unowned self] (noti) in
            guard let value = noti.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue else {
                return
            }
            self.keyboardRect = value.cgRectValue
        })
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// Binding
extension LoginViewController {
    
    func binding() {
        
        userTextField.bnd_text.bidirectionalBind(to: viewModel.username)
        passwordTextField.bnd_text.bidirectionalBind(to: viewModel.password)
        bnd_bag.add(disposable: loginButton.bnd_tap.observeNext {
                self.viewModel.login()
                self.viewModel.user?.completeBlock = { ( data: DataResponse<Any>? ) in
                    self.performSegue(withIdentifier: "main", sender: nil)
                }
            }
        )

        viewModel.enableLogin.bind(to: loginButton.bnd_isEnabled)
        bnd_bag.add(disposable: viewModel.enableLogin
            .observeNext { (result) in
                let color = result ? UIColor.zoneSquash : UIColor.zoneSilver
                let textColor = result ? UIColor.zoneBlack : UIColor.zoneBlack
                self.loginButton.backgroundColor = color
                self.loginButton.borderColor = result ? UIColor.zoneSquash : UIColor.zoneSilver
                self.loginButton.setTitleColor(textColor, for: .normal)
                }
        )
    }
}
