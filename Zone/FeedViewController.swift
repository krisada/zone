//
//  FeedViewController.swift
//  Zone
//
//  Created by Developer on 11/2/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import MXSegmentedPager
import ReactiveKit


class FeedViewController: MXSegmentedPagerController {
    
    let headerView = FeedHeaderView.instanceFromNib()
    
    var viewModel: FeedViewModel!
    
    
    var sectionsViewController: [FeedSectionTableViewController] {
        
        func initWithViewModel(viewModel: FeedSectionViewModel) -> FeedSectionTableViewController? {
            let menuStoryboard = UIStoryboard(name: "Feed", bundle: nil)
            if let vc = menuStoryboard.instantiateViewController(withIdentifier: "Board") as? FeedSectionTableViewController {
                vc.viewModel = viewModel
                return vc
            }
            return nil
        }
        
        return viewModel.sections.map { initWithViewModel(viewModel: $0) ?? FeedSectionTableViewController() }
    }
    

    override func viewDidLoad() {
        
        /* Setup */
        super.viewDidLoad()
        self.viewModel = FeedViewModel()
        self.setupHeader()
        
        /* Setup Opertaion*/
        let locationOp = CurrentLocationManager.sharedInstance
        let zoneLocationOp = ZoneLocation()
        zoneLocationOp.addDependency(locationOp)
        
        locationQueue.addOperation(locationOp)
        locationQueue.addOperation(zoneLocationOp)
        locationQueue.addOperation { [unowned self] in
            
            let lat = CurrentLocationManager.currentLocation?.coordinate.latitude ?? 0
            let lon = CurrentLocationManager.currentLocation?.coordinate.longitude ?? 0
            let id = zoneLocationOp.location?.id ?? ""
            
            let zone = Zone.CurrentLocation(id ,lat, lon)
            self.viewModel.loadData(zone: zone)
            self.bind(location: zoneLocationOp.location)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hideBG = true
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension FeedViewController {
    
    func setupHeader() {
        headerView.delegate = self
        
        segmentedPager.backgroundColor = UIColor.white

        /* Parallax Header */
        segmentedPager.parallaxHeader.view = headerView
        segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.fill;
        segmentedPager.parallaxHeader.height = UIScreen.main.bounds.width / 2
        segmentedPager.parallaxHeader.minimumHeight = 75
        
        /* Segmented Control customization */
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down;
        segmentedPager.segmentedControl.backgroundColor = UIColor.black
        segmentedPager.segmentedControl.titleTextAttributes = [
            NSFontAttributeName : UIFont.zoneSegmentNormalFont(),
            NSForegroundColorAttributeName : UIColor.zonePinkishGrey
        ];
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [
            NSFontAttributeName : UIFont.zoneSegmentSelectedFont(),
            NSForegroundColorAttributeName : UIColor.zoneWhiteThree
        ]
        segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.orange
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
    }
    
    func bind(location: Location? = nil) {
        bnd_bag.add(disposable:(User.shareInstance.userMe?.imageProfile.observeNext { [unowned self] value in
            if let url = URL(string: value) {
                DispatchQueue.main.async {
                    self.headerView.userImage.sd_setImage(with: url)
                }
            }
            })!)
        
        if let loc = location {
            bnd_bag.add(disposable:(loc.image.observeNext { [unowned self] value in
                if let url = URL(string: value) {
                    DispatchQueue.main.async {
                        self.headerView.bgImage.sd_setImage(with: url)
                    }
                }
            }))
            bnd_bag.add(disposable:(loc.name.observeNext { [unowned self] value in
                DispatchQueue.main.async {
                    self.headerView.selectZoneButton.setTitle(value, for: .normal)
                }
            }))
            bnd_bag.add(disposable:(loc.members.observeNext { [unowned self] value in
                DispatchQueue.main.async {
                    self.headerView.memberCountLabel.text = "\(value) Visitors"
                }
            }))
        }
    }
    
}

// Segment Controller //
extension FeedViewController {
    
    override func heightForSegmentedControl(in segmentedPager: MXSegmentedPager) -> CGFloat {
        return 33
    }
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return viewModel.sections.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return viewModel.sections[index].title
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        return sectionsViewController[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        
        if (parallaxHeader.view?.frame.size.height)! <= CGFloat(90) {
            headerView.showBack(true)
        } else {
            headerView.showBack(false)
        }
    }
}

extension FeedViewController: FeedHeaderDelegate {
    
    func feedHeaderSelected(_ type: FeedHeaderType) {
        guard let vc = type.getViewController() else {
            return
        }
        
        if let menuVC = vc as? MenuViewController {
            menuVC.delegate = self
        }
        present(vc, animated: true, completion: nil)
    }
}

extension FeedViewController: MenuViewControllerDelegate {
    
    func menuDidSelect(menu: MenuSetting) {
        navigationController?.pushViewController(menu.viewController, animated: true)
    }
}
