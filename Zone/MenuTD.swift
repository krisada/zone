//
//  MenuTD.swift
//  RetailStore
//
//  Created by Krisada Kaewjunchai on 11/26/2557 BE.
//  Copyright (c) 2557 Krisada.K. All rights reserved.
//

import UIKit

class MenuTD: NSObject, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    
    fileprivate var isPresenting = true

    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return MenuPC(presentedViewController: presented, presenting: presenting)
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animatePresentationWithTransitionContext(_ transitionContext: UIViewControllerContextTransitioning) {
        let presentedController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let presentingController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        let presentingControllerView = presentingController?.view
        let containerView = transitionContext.containerView
        
        // Position the presented view off the top of the container view
        presentedControllerView.frame = transitionContext.finalFrame(for: presentedController!)
        presentedControllerView.center.x -= containerView.bounds.size.width
        
        containerView.addSubview(presentedControllerView)
        
        // Animate the presented view to it's final position
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 0.0,
                       options: .allowUserInteraction,
                       animations: {
            presentedControllerView.center.x += containerView.bounds.size.width
            presentingControllerView!.center.x += presentedControllerView.frame.width
            }, completion: {(completed: Bool) -> Void in
                
                // Check Transition was cancelled or not.
                if transitionContext.transitionWasCancelled {
                    transitionContext.completeTransition(false)
                } else {
                    transitionContext.completeTransition(true)
                }
                
        })
    }
    
    func animateDismissalWithTransitionContext(_ transitionContext: UIViewControllerContextTransitioning) {
        let presentingController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let presentingControllerView = presentingController?.view
        let containerView = transitionContext.containerView
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 0.0,
                       options: .allowUserInteraction,
                       animations: {
            presentedControllerView.center.x -= containerView.bounds.size.width
            presentingControllerView!.center.x -= presentedControllerView.frame.width
            }, completion: {(completed: Bool) -> Void in
                // Check Transition was cancelled or not.
                if transitionContext.transitionWasCancelled {
                    transitionContext.completeTransition(false)
                } else {
                    transitionContext.completeTransition(true)
                }
        })
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animatePresentationWithTransitionContext(transitionContext)
        }
        else {
            animateDismissalWithTransitionContext(transitionContext)
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        return self
    }

}
