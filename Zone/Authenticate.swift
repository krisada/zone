//
//  Login.swift
//  Zone
//
//  Created by Apple on 12/3/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper


class Authenticate: AsyncOperation {
    
    var completeBlock: ((DataResponse<Any>?) -> (Void))?
    var username = ""
    var password = ""
    
    init(user: String = "", pass: String = "") {
        
        self.username = user
        self.password = pass
    }
    
    override func main() {

        let parameters: Parameters = ["client_id" : "55c1f1345f5e8824008b4567",
                                      "client_secret" : "rsbsUmr7VTnZUV9ctyfD5JG9xwsrJRRiR6VkrsTO",
                                      "grant_type" : "password",
                                      "username" : self.username,
                                      "password" : self.password
                                     ]
        
        ZoneAPI.request(ZoneURL.auth, method: .post, mappableType: AccessToken.self, parameters: parameters) { (obj: Mappable?, error: NSError?) in
            
            if let objToken = obj as? AccessToken, let tokenString = objToken.accessToken {
                UserDefaults.setAccessToken(token: tokenString)
                self.completeBlock?(nil)
                
                // Load User Profile //
                ZoneAPI.request(ZoneURL.userProfile, method: .get, mappableType: UserMe.self, completeBlock: { (obj: Mappable?, error: NSError?) in
                    if let userObject = obj as? UserMe {
                        User.shareInstance.userMe = userObject
                        User.shareInstance.complete?()
                    }
                })
            }
            self.state = .Finished
        }
    }
}
