//
//  FeedHeaderView.swift
//  Zone
//
//  Created by Developer on 11/2/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


// Feed Header Type
// Feed Header Factory
enum FeedHeaderType {
    
    static let staticMenuTD = MenuTD()
    
    case menu
    
    case changeLocation
    
    case search
    
    case map
    
    case joinZone
    
    
    func getViewController() -> UIViewController? {
        switch self {
            
        case .menu:
            let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
            guard let vc = menuStoryboard.instantiateInitialViewController() else {
                return nil
            }
            vc.transitioningDelegate = FeedHeaderType.staticMenuTD
            vc.modalPresentationStyle = .custom
            return vc
            
        case .changeLocation:
            let menuStoryboard = UIStoryboard(name: "Select Location", bundle: nil)
            guard let vc = menuStoryboard.instantiateInitialViewController() else {
                return nil
            }
            return vc
            
        default:
            return nil
        }
    }
    
}


protocol FeedHeaderDelegate {
    
    func feedHeaderSelected(_ type: FeedHeaderType)
}


class FeedHeaderView: UIView {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var memberCountLabel: UILabel!
    @IBOutlet weak var selectZoneButton: UIButton!
    
    var delegate: FeedHeaderDelegate?
    
    class func instanceFromNib() -> FeedHeaderView {
        return UINib(nibName: "FeedHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FeedHeaderView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func menu(_ sender: Any) {
        delegate?.feedHeaderSelected(.menu)
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        delegate?.feedHeaderSelected(.changeLocation)
    }
    
    func showBack(_ show: Bool) {
        if show {
            
            UIView.animate(withDuration: 0.3, animations: { [unowned self] in
                self.blackView.alpha = 1
            })
        } else {
            
            UIView.animate(withDuration: 0.3, animations: { [unowned self] in
                self.blackView.alpha = 0
            })
        }
    }
    
}
