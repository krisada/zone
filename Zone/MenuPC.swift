//
//  MenuPC.swift
//  RetailStore
//
//  Created by Krisada Kaewjunchai on 11/26/2557 BE.
//  Copyright (c) 2557 Krisada.K. All rights reserved.
//

import UIKit

class MenuPC: UIPresentationController {
    
    let dimmingView = UIView()
    var tapGesture:UITapGestureRecognizer!
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(MenuPC.onTapOutSide(_:)))
        self.dimmingView.addGestureRecognizer(self.tapGesture)
    }
    
    func onTapOutSide(_ sender:UITapGestureRecognizer) {
        presentingViewController.dismiss(animated: true, completion: nil)
    }
    
    override func presentationTransitionWillBegin() {
        dimmingView.frame = containerView!.bounds
        dimmingView.alpha = 0
        presentedViewController.view.alpha = 0
        containerView!.insertSubview(dimmingView, at: 0)
        
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { context in
            self.presentedViewController.view.alpha = 1
            self.presentedViewController.view.layer.shadowColor = UIColor.black.cgColor
            self.presentedViewController.view.layer.shadowOffset = CGSize(width: 12, height: 12)
            self.presentedViewController.view.layer.shadowRadius = 12
            self.presentedViewController.view.layer.shadowOpacity = 0.5
            self.dimmingView.alpha = 1.0
            }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: {context in
            self.dimmingView.alpha = 0.0
            }, completion: nil)
    }
    
    override var frameOfPresentedViewInContainerView : CGRect {
        
        let menuFrame = CGRect(x: 0, y: 0, width: containerView!.bounds.width - 40, height: containerView!.bounds.height)
        return menuFrame
    }
    
    deinit {
        self.dimmingView.removeFromSuperview()
    }
}
