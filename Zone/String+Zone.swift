//
//  String+Validatation.swift
//  Zone
//
//  Created by Developer on 10/22/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import Foundation

// Validation //
extension String {
    
    func isPhone() -> Bool {
        let reg = "^0[0-9]{9}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reg)
        return predicate.evaluate(with: self)
    }
    
    func isName() -> Bool {
        let reg = "^([a-zA-Zก-ฺเ-๙])([0-9a-zA-Zก-ฺเ-๙\\ \\.\\_]){1,}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reg)
        return predicate.evaluate(with: self)
    }
    
    func isPassword() -> Bool {
        let reg = "^[a-zA-Z0-9]{6,20}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", reg)
        return predicate.evaluate(with: self)
    }
    
}
