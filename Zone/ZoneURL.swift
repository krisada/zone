//
//  APIURL.swift
//  Zone
//
//  Created by Apple on 1/7/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation

struct ZoneURL {
    
    private static let base = "http://api.enegist.net"
    private static let version = "/latest"
    
    // Core //
    static let auth = base + version + "/core/oauth/accesstoken"
    static let userProfile = base + version + "/core/user/me"
    
    // Location //
    
    /* let, long, (id) -> location detail (name, id, cover image, stack list)*/
    static let location = base + version + "/location/explore"
    
    /* Get current localtion with type (area, place) */
    static let current = base + version + "/location/current"
    static let currentSuggest = base + version + "/location/current/suggest"
    
    
    // Social //
    static let feed = base + version + "/social/feed"
}

enum ZoneErrorType {
    
    case Unknown
    case Authen
    case Error(String)
    
    private var name: String {
        
        switch self {
        case .Authen:
            return "Authen"
        case .Error(let message):
            return "Error: \(message)"
        case .Unknown:
            return "Unknown"
        }
    }
    
    var error: NSError {
        return NSError(domain: self.name, code: 0, userInfo: nil)
    }
}
