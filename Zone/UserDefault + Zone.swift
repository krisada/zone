//
//  UserDefault + Zone.swift
//  Zone
//
//  Created by Apple on 1/8/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation


extension UserDefaults {
    
    static let kAccessToken = "ACCESS_TOKEN"
    
    static func getAccessToken() -> String? {
        let userDefault = UserDefaults.standard
        return userDefault.string(forKey: UserDefaults.kAccessToken)
    }
    
    static func setAccessToken(token :String) -> Void {
        let userDefault = UserDefaults.standard
        userDefault.set(token, forKey: UserDefaults.kAccessToken)
        userDefault.synchronize()
    }
}
