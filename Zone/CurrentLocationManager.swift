//
//  LocationManager.swift
//  Zone
//
//  Created by Apple on 1/23/17.
//  Copyright © 2017 Enegist. All rights reserved.
//

import Foundation
import CoreLocation


class CurrentLocationManager: AsyncOperation {
    
    let locationManager = CLLocationManager()
    
    static var currentLocation: CLLocation?
    
    static let sharedInstance = CurrentLocationManager()
    
    
    override init() {
        super.init()
    }
    
    override func main() {
        if CurrentLocationManager.currentLocation?.coordinate.latitude == 0 {
            self.state = .Finished
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        let code = CLLocationManager.authorizationStatus()
        switch code {
        case .denied:
            break;
        default:
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
        }
    }
}

extension CurrentLocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            CurrentLocationManager.currentLocation = location
            self.state = .Finished
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
