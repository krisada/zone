//
//  PostFilterTableViewCell.swift
//  Zone
//
//  Created by Krisada Keawjunchai on 11/9/16.
//  Copyright © 2016 Enegist. All rights reserved.
//

import UIKit


protocol PostFilterDelegate {
    
}

class PostFilterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        let disposable = User.shareInstance.userMe?.imageProfile.observeNext { [unowned self] value in
            if let url = URL(string: value) {
                DispatchQueue.main.async {
                    self.userImage.sd_setImage(with: url)
                }
            }
        }
        if let disposable = disposable {
            bnd_bag.add(disposable:disposable)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func filter(_ sender: Any) {
    }
    
    @IBAction func post(_ sender: Any) {
    }
    
}
